"""
server.py deals with routes and handles the responses
"""

from http.server import BaseHTTPRequestHandler
from pathlib import Path
import time

from routes.main import routes


class Server(BaseHTTPRequestHandler):
    """
    Class
    """

    def do_GET(self):
        """kicks off this process when a GET request is received."""
        self.respond()

    def handle_http(self):
        """send our basic http handlers and then return the content."""

        content_type = "text/plain"
        response_content = ""
        if self.path in routes:
            status = 200
            print(routes[self.path])
            route_content = routes[self.path]["template"]
            filepath = Path("templates/{}".format(route_content))
            # print(filepath)
            if filepath.is_file():
                if str(filepath).endswith("index.html"):
                    content_type = "text/html"
                elif str(filepath).endswith("json"):
                    content_type = "application/json"
                elif str(filepath).endswith("txt"):
                    content_type = "text/plain"
                elif str(filepath).endswith("status.html"):
                    content_type = "text/html"
                    if not self.path.endswith("/status"):  #  Any ID
                        status = int(self.path[-3:])
                elif str(filepath).endswith("delay.html"):
                    content_type = "text/html"
                    # Creating delay
                    time.sleep(int(self.path[-1]))
                response_content = open(
                    "templates/{}".format(route_content)
                )  # Open file
                response_content = response_content.read()  # Read file

            else:
                content_type = "text/plain"
                status = 404
                response_content = "404 Not Found"
        else:
            content_type = "text/plain"
            status = 404
            response_content = "404 Not Found"

        self.send_response(status)
        self.send_header("Content-type", content_type)
        self.end_headers()
        return bytes(response_content, "UTF-8")

    def respond(self):
        """sending the actual response out"""
        content = self.handle_http()
        self.wfile.write(content)
