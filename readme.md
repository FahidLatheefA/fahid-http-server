# Fahid's HTTP Server

This project uses basic python inbuilt modules to host a HTTP server. The server changes based on changing URL routes.

## Setup

### Prerequisites

Make sure you have these installed in your system

1. &nbsp;python3

```
sudo apt install python3.8
```

2. &nbsp;pip3

```
sudo apt-get -y install python3-pip
```

3. &nbsp;virtualenv

```
pip3 install virtualenv
```

### Installation

To run the project in your system, follow these instructions.

1)&nbsp;Clone the project

- Clone with SSH

```shell
git@gitlab.com:FahidLatheefA/fahid-http-server.git
```

- Clone with HTTPS

```shell
https://gitlab.com/FahidLatheefA/fahid-http-server.git
```

2)&nbsp;Create and activate a new virtual environment

```shell
virtualenv /path/to/new/virtual/environment
```

```shell
source /path/to/new/virtual/environment/bin/activate
```

3)&nbsp;Run the project from root directory

Navigate to project root folder and run

```shell
python3 main.py
```

Open localhost:8000 in your browser

## Project usage

- Try adding routes to the URL such as `/home`, `/json`, `/uuid`, `/server/300`, `/delay/3` etc. and see the magic.

Author: Fahid Latheef A
Date: September 17, 2021
