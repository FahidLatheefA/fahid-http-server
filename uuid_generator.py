import uuid

myuuid = uuid.uuid4()

with open("templates/uuid.txt", "w", encoding="utf-8") as f:
    # perform file operations
    f.write('{\n    "uuid": ')
    f.write('"')
    f.write(str(myuuid))
    f.write('"')
    f.write("\n}")
